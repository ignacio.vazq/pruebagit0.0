<?php
  include ('conexion.php');

  class coche{
// funcion para insertat
    public function insertar($nomCliente, $argdni, $argdireccion, $argTel, $argTelconta){ //nombre de los post
      $modelo = new conexion();
      $conexion = $modelo -> get_conection();
      $sql = "INSERT INTO cliente(cliente.conductor,cliente.dni,cliente.direcciontelefono,cliente.telefono_contacto) values (:nomConductor, :DNI, :tipo, :subtipo)";
      $statement = $conexion -> prepare($sql);
      $statement -> bindValue(':nomConductor',$nomCliente,PDO::PARAM_INT);
      $statement -> bindParam(':DNI', $argdni,PDO::PARAM_INT);
      $statement -> bindParam(':direccion', $argDireccion,PDO::PARAM_INT);
      $statement -> bindParam(':telefono', $argTel,PDO::PARAM_INT);
      $statement -> bindParam(':telefonoContacto', $argTelconta,PDO::PARAM_INT);
      if (!$statement){
        return 'error a crear el registro';
      }else{
        $statement->execute();
        return 'Registro creado correctamente';
      }
  }
}
