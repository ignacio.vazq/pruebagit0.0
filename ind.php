<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> SELECT </title>
  </head>
  <body>
    <h1> ID </h1>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <div class="col-12 mt-3">
      <table class="table table-striped text-center">
        <thread>
          <tr>
            <th> ID </th>
            <th> Conductor </th>
            <th> DNI </th>
            <th> Dirección </th>
            <th> telefono </th>
            <th> telefono contacto </th>
          </tr>

          <?php
          //include 'busqueda.php';

            $busqueda = $_GET["buscar"]; //direccionar

            try {
              $base = new PDO('mysql:host=localhost; dbname=exaii', 'root', '');
              //$base -> setAtribute(PDO::ATTR_ERRMODE, PDO::ERRNODE_EXCEPTION);
              //$base -> exec("SET CHARACTER SET utf8");
                  //echo $busqueda;
              $sql = "SELECT cliente.id_cliente, cliente.conductor, cliente.dni, cliente.direccion, cliente.telefono, cliente.telefono_contacto
               FROM cliente WHERE cliente.id_cliente <= ? ";
              $resultado = $base -> prepare($sql);
              $resultado -> execute(array($busqueda)); //ejecuta la sentencia preparada

              while ($registro = $resultado -> fetch(PDO::FETCH_ASSOC)) {
                ?>

                <tr>
                  <td><?php echo"". $registro['id_cliente']; ?></td>
                  <td><?php echo"".$registro['conductor']; ?></td>
                  <td><?php echo"".$registro['dni']; ?></td>
                  <td><?php echo"".$registro['direccion']; ?></td>
                  <td><?php echo"".$registro['telefono']; ?></td>
                  <td><?php echo"".$registro['telefono_contacto']; ?></td>
                </tr>

                <?php

              }
              $resultado -> closeCursor(); //cerrar la tabla para no utilizar recursos
            } catch (Exception $e) {
                die('Error: '. $e->GetMessage());
            }finally{
              $base = null; //cerrar conexion a la bsae de datos
            }

           ?>
        </thread>
      </table>

    </div>
    <br><a class="btn btn-primary" href="busqueda.php" role="button"> Regresar </a>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
