use EXAII;

INSERT INTO oficina (ciudad,direccion,telefono) VALUES
('cataluña','avenida san fermin 23 24880',5589230942),
('Sevilla','calle del norte 13 45890', 5489234598);

/*en este caso a la hora de insertar datos se omite la llave primaria ya que se ddefinió como AUTO_INCREMENT*/
INSERT INTO coche (matricula,grupo,marca,modelo,puertas,plazas,capacidad,edad_minima,id_oficina) VALUES
('MZK-23-89','A','GMC','2019',5,5,100,25,1),
('AIJ-45-62','B','Toyota','2016',2,5,150,19,1),
('PLJ-11-02','C','Ford','2018',5,8,200,18,2),
('MLS-03-99','D','Nissan','2017',5,5,150,20,2);

INSERT INTO cliente (conductor,dni,direccion,telefono,telefono_contacto) VALUES
('Jose Robles Villalpando','JRV123I3','Calle de las sirenas 12','5442939223','93781293'),
('Jonathan Bree','JB876F9','Calle libertad 1','8742939245','6378120778'),
('Rosalinda Schuls','RS825B65','Avenida revolucion 13','0942895345','6579842589');

INSERT INTO alquila (duracion,seguro,precio_total,id_cliente,id_coche) VALUES
(7,'completo',2400,1,1),
(15,'contra robos',6000.89,2,2),
(9,'contra daños',4900,3,3);
