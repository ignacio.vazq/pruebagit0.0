CREATE DATABASE EXAII;

CREATE table oficina(
  id_oficina int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ciudad VARCHAR(50),
  direccion VARCHAR(200),
  telefono VARCHAR(20)
);

CREATE TABLE coche(
  id_coche int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  matricula VARCHAR(10),
  grupo char(1),
  marca VARCHAR(20),
  modelo VARCHAR(4),
  puertas int,
  plazas int,
  capacidad int,
  edad_minima int,
  id_oficina int,
  foreign key (id_oficina) REFERENCES oficina(id_oficina)
);

create table cliente(
  id_cliente int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  conductor VARCHAR(30),
  dni VARCHAR(20),
  direccion VARCHAR(100),
  telefono  VARCHAR(20),
  telefono_contacto VARCHAR(20)
);

CREATE TABLE alquila(
  id_alquila int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  duracion int,
  seguro VARCHAR(30),
  precio_total float,
  id_cliente int,
  id_coche int,
  FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
  FOREIGN KEY (id_coche) REFERENCES coche(id_coche)
);
